# Swiss Annoyances List

Subscribe to https://dns2utf8.gitlab.io/ch-annoyances/dns2utf8-swiss.txt

## How to install

1. Copy the link to your Clipboard: https://dns2utf8.gitlab.io/ch-annoyances/dns2utf8-swiss.txt
2. Open uBlock Origin Settings
3. Click on "Filter Lists"
4. Click on "Import ..."
5. Paste the URL https://dns2utf8.gitlab.io/ch-annoyances/dns2utf8-swiss.txt of this list into the box
6. Click "Apply"

![Installation Steps](Installation_Steps.png)

